<?php
function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();

    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'popper-scripts', get_template_directory_uri() . '/js/popper.min.js', array(), false);
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
}



    function getTemplate( $path, $post ){


        ob_start();
        $post;
        include($path);
        $html = ob_clean();

        return $html;


    }
 
add_image_size( 'grid-post-thumbnail', 314, 180, true );

    function vd($input){

        echo "<pre>";
        var_dump($input);
        echo "</pre>";

    }

    require_once('lib/TemplateEngine.php');
    require_once('lib/wwMenuWalker.php');


    function wwOrganizationJson(){

        $wwOrg = [

            '@context'	=>	'http://schema.org',
            '@type'		=>	'Organization',
            'name'		=>	'WeaponWear Concealment',
            'url'		=>	'https://www.weaponwearconcealment.com',
            'email'		=>	'info@weaponwearconcealment.com',
            'telephone'	=>	'855-380-1911',
            'logo'		=>	get_stylesheet_directory_uri() . '/images/weapon_wear_concealment_logo.jpg',
            'description'	=>	'#1 source for personal protection accessories.'
    
            ];

        return $wwOrg;
    }
    // add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

    // function new_loop_shop_per_page( $cols ) {
    //   // $cols contains the current number of products per page based on the value stored on Options -> Reading
    //   // Return the number of products you wanna show per page.
    //   $cols = 5;
    //   return $cols;
    // }
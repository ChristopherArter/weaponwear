<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<script type="application/ld+json">
				<?php echo json_encode(wwOrganizationJson()); ?>
				</script>
	<?php wp_head(); ?>
	<style>
	
.woocommerce-Price-amount {
	color:#333;
}

	</style>
</head>

<body itemscope itemtype="http://schema.org/WebPage" <?php body_class(); ?>>
<meta itemprop="about" content="<?php bloginfo( 'description' ); ?>">

<div class="hfeed site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<div class="wrapper-fluid wrapper-navbar" id="wrapper-navbar">

		<a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e( 'Skip to content',
		'understrap' ); ?></a>

		<nav class="navbar navbar-expand-md navbar-dark bg-dark">

		<?php if ( 'container' == $container ) : ?>
			<div class="container">
		<?php endif; ?>

					<!-- CUSTOM LOGO -->
					<a class="navbar-brand" href="<?php echo get_home_url(); ?>">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/weapon_wear_concealment_logo.svg" width="250" alt="weapon-wear-concealment-logo">
				  </a>









				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<!-- The WordPress Menu goes here -->
				<?php wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'collapse navbar-collapse',
						'container_id'    => 'navbarNavDropdown',
						'menu_class'      => 'navbar-nav',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'walker'          => new wwMenuWalker(),
					)
				); ?>
	
			<?php if ( 'container' == $container ) : ?>
			<form role="search" method="get" class="form-inline" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
	<label class="screen-reader-text" for="s"><?php _e( 'Search for:', 'woocommerce' ); ?></label>
	<input type="search" class="form-control mr-sm-2" placeholder="<?php echo esc_attr_x( 'Search Products&hellip;', 'placeholder', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'woocommerce' ); ?>" />
	
	<input type="hidden" name="post_type" value="product" />
</form>
<ul class="navbar-nav">
<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle btn btn-xs" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         My Account
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
		<a class="dropdown-item" href="<?php get_site_url(); ?>/account">Account</a>
          <a class="dropdown-item" href="<?php get_site_url(); ?>/orders">Orders</a>
		  <a class="dropdown-item" href="<?php get_site_url(); ?>/shopping-cart">Cart</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
	  </ul>
			</div><!-- .container -->
			<?php endif; ?>

		</nav><!-- .site-navigation -->
	</div><!-- .wrapper-navbar end -->

<?php 

    $previousLink = get_previous_posts_page_link($posts->max_num_pages);
    $nextLink = get_next_posts_page_link( $posts->max_num_pages );
?>

<nav aria-label="Page navigation example">
  <ul class="pagination">

  <?php if(! previous_posts_link( '&laquo; PREV', $loop->max_num_pages) == ""){
      ?>
    <li class="float-right page-item"><a class="page-link" href="<?php echo $previousLink; ?>"><strong><<</strong></a></li>
      <?php 
  } ?>

<?php if(next_posts_link( 'NEXT &raquo;', $loop->max_num_pages)){
      ?>
<li class="page-item"><a class="page-link" href="<?php echo $nextLink; ?>"><strong>>></strong></a></li>
      <?php 
  } ?>

  </ul>
</nav>
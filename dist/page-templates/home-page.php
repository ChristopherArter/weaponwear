<?php
/**
 * Template Name: Home Template
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="full-width-page-wrapper">

	<div class="container-fluid" style="background:#444; margin-top:-30px !important; padding:15px; color:#fff;">
		<div class="container">
		<div id="carouselExampleIndicators" class="carousel slide" >
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
<?php
	$featArgs = [
		'post_type'	=>	'product',
		'post_status'	=>	'publish',
		'posts_per_page'	=>	10,
		'tax_query' => [
				[   
					'taxonomy' => 'product_visibility',
					'field'    => 'name',
					'terms'    => 'featured',
				],
		],
	];

	$featArgsFallback = [
		'post_type'	=>	'product',
		'post_status'	=>	'publish',
		'posts_per_page'	=>	10,
		'order_by'	=>	'rand'
	];

	$featuredPosts = new WP_Query($featArgs);

	if(!$featuredPosts->have_posts()){
		$featuredPosts = new WP_Query($featArgsFallback);
	}
?>

<?php 


	if($featuredPosts->have_posts()){
		$featCount = 1;
		
		while($featuredPosts->have_posts()){
			
			$featuredPosts->the_post();
			$featImage = wp_get_attachment_image_src( get_post_thumbnail_id( $product->ID ), 'large' );
			$price = get_post_meta( get_the_ID(), '_regular_price', true);
			$sale = get_post_meta( get_the_ID(), '_sale_price', true);
			$productLink = get_permalink(get_the_id());
			?>

<div class="carousel-item <?php if($featCount == 1){ echo "active"; }?>">
  
   <div class="row">
 		<div class="col-md-3">
		 <a href="<?php echo $productLink; ?>"><img class="rounded thumbnail" src="<?php echo $featImage[0]; ?>" alt="weaponwear_<?php echo $product->get_the; ?>_<?php echo $featImage[1]; ?>x<?php echo $featImage[2]; ?>_1">
 		</a>
 		</div>
 		<div class="col-md-6">
		<h2><?php echo $product->get_title(); ?></h2>
		 <p><?php 

if($post->post_excerpt){
	echo $post->post_excerpt;
} else {

	echo $post->post_content;
} ?></p><p><?php
if($sale){ ?>


	<del><span class="text-muted"><?php echo "\$"; echo $price; ?></span></del> <strong>On sale!</strong> <span class="badge badge-danger"><?php echo $currency; echo $sale; ?></span>

<?php
} elseif($price) { 

// if it's NOT on sale
	?>

<span class="badge badge-light"><?php echo "\$"; echo $price; ?></span>

<?php } ?></p>
			 <a href="<?php echo get_permalink(get_the_id()); ?>" class="btn btn-md btn-outline-primary">View</a>
			<a href="/?add-to-cart=<?php echo get_the_id(); ?>" class="btn btn-md btn-primary">Add to Cart</a>
 		</div>
	</div>

	<?php 
	$wwOrg = [

		'@context'	=>	'http://schema.org',
		'@type'		=>	'Organization',
		'name'		=>	'WeaponWear Concealment',
		'url'		=>	'https://www.weaponwearconcealment.com',
		'email'		=>	'info@weaponwearconcealment.com',
		'telephone'	=>	'855-380-1911',
		'logo'		=>	get_stylesheet_directory_uri() . '/images/weapon_wear_concealment_logo.jpg',
		'description'	=>	'#1 source for personal protection accessories.'

		];


if($product->post_excerpt){

		  $jsonDesc = $product->post_excerpt;
	 
	 } else {

		  $jsonDesc = $product->post_content;
	  } 



$productJson = [

'@context'		=>	'http://schema.org',
'@type'			=>	'Product',
'name'			=>	$product->get_title(),
'offers'		=>	[

	// schema.org/Offer
	'@context'		=>	'http://schema.org',
	'@type'			=>	'Offer',
	'availability'	=>	'https://schema.org/InStock',
	'price'			=>	$jsonPrice,
	'url'			=>	get_permalink(get_the_id()),
	'seller'		=>	$wwOrg,
	'offeredBy'		=>	$wwOrg,
	'description'	=>	$jsonDesc,
	'image'			=>	$featImage[0],
	'priceCurrency'	=>	'USD'
	],

'description'	=>	$jsonDesc,

];

?>
<script type="application/ld+json">
<?php echo json_encode($productJson); ?>

</script>

</div>


			<?php 
			$featCount ++;
		}
	}

?>
  </div>
</div>
  <a class="carousel-control-prev"  style="z-index:-999; color:#333;"  href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" style="z-index:-999;  color:#333;" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
	</div>
</div>

<div itemprop="mainContentOfPage" type="http://schema.org/WebPageElement" class="wrapper" id="woocommerce-wrapper">
	<div class="container" id="content">
		<div class="row justify-content-md-center">
		<div class="col-md-auto">
		<div class="alert" style="background-color:#f7f7f7; color:#333;" role="alert">
  <h5><strong><i class="fa fa-2x fa-truck" aria-hidden="true"></i> Free Priority Shipping on all domestic US orders!</strong></h5>
</div>
</div>
	</div>
		<div class="row">
			
			<div class="col-md-8 content-area" id="primary">
				<main class="site-main" id="main" role="main">

				<h5>Our Latest Deals!</h5>
			
						<div class="card-deck">
						<?php 

						$productArgs = [

							'post_type'		=>	'product',
							'post_status'	=>	'publish',
							'posts_per_page'	=>	-1,
							'meta_query'     => array(
								'relation' => 'OR',
								array( // Simple products type
									'key'           => '_sale_price',
									'value'         => 0,
									'compare'       => '>',
									'type'          => 'numeric'
								),
								array( // Variable products type
									'key'           => '_min_variation_sale_price',
									'value'         => 0,
									'compare'       => '>',
									'type'          => 'numeric'))

						];

						$products = new WP_Query($productArgs);
						
						if( $products->have_posts() ){

							while($products->have_posts()){

								$products->the_post();
						
								?>

<div class="col-md-4" style="margin-top:15px;">
								<?php 
									
								include(get_stylesheet_directory().'/includes/product_in_loop.php');
								?>
								</div>
								<?php 
							}

						}

?>

						</div><!-- end card deck -->
			



				</main><!-- #main -->

			</div><!-- #primary -->
					
<?php get_sidebar('right'); ?>

		</div><!-- .row end -->

	</div><!-- Container end -->

</div><!-- Wrapper end -->
</div>
<?php get_footer(); ?>

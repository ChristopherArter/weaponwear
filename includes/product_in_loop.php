<?php 	
	/*
		
		Set template variables

	 */
	$featImage = get_the_post_thumbnail( $post->ID, '190x109', array('class'=>'card-img-top' , 'alt' => $product->get_title()));
	$schemaThumb = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'grid-post-thumbnail' );
	$attachment_ids = $product->get_gallery_attachment_ids();

							 ?>

 <div class="card" >
 <a href="<?php echo get_permalink(get_the_id()); ?>">
<?php echo $featImage; ?>
</a>
								
<?php


	/*
	
		PRODUCT BODY

	 */


?>

				
<div class="card-body">



	<!-- PRODUCT TITLE -->
	<a href="<?php echo get_permalink(get_the_id()); ?>"><h6 class="card-title"><?php echo $product->get_title(); ?></h6></a>
				  <h6 class="card-subtitle mb-2">




	<?php 

	// Product Price

		$currency = get_woocommerce_currency_symbol();
		$price = get_post_meta( get_the_ID(), '_regular_price', true);
		$sale = get_post_meta( get_the_ID(), '_sale_price', true);

		// if it's on sale
		if($sale){ ?>


			<del><span class="text-muted"><?php echo $currency; echo $price; ?></span></del> <strong>On sale!</strong> <span class="badge badge-danger"><?php echo $currency; echo $sale; ?></span>
		
		<?php
		} elseif($price) { 

		// if it's NOT on sale
			?>

		<span class="badge badge-light"><?php echo $currency; echo $price; ?></span>

		<?php } ?></h6>
		

		<!-- VIEW & ADD TO CART -->	      
		<p class="card-text">
			<a href="<?php echo get_permalink(get_the_id()); ?>" class="btn btn-sm btn-outline-primary">View</a>
			<a href="/?add-to-cart=<?php echo get_the_id(); ?>" class="btn btn-sm btn-primary">Add to Cart</a>
		<p><?php 

				  if($product->post_excerpt){
					  echo $product->post_excerpt;
				  } else {

					  echo $product->post_content;
				  } ?>
					  
				  </p>

				</div>

<?php 


	/*
	

			PRODUCT FOOTER

	 */


?>

</div> <!-- /.card -->



	<?php

	/*
	
		PRODUCT SCHEMA

	 */
	
	if($sale) {

		$jsonPrice = $sale;
	} else {

		$jsonPrice = $price;
	}

		$wwOrg = [

					'@context'	=>	'http://schema.org',
					'@type'		=>	'Organization',
					'name'		=>	'WeaponWear Concealment',
					'url'		=>	'https://www.weaponwearconcealment.com',
					'email'		=>	'info@weaponwearconcealment.com',
					'telephone'	=>	'855-380-1911',
					'logo'		=>	get_stylesheet_directory_uri() . '/images/weapon_wear_concealment_logo.jpg',
					'description'	=>	'#1 source for personal protection accessories.'

					];
		

		if($product->post_excerpt){

					  $jsonDesc = $product->post_excerpt;
				 
				 } else {

					  $jsonDesc = $product->post_content;
				  } 

		$jsonProductCategories = [];
		$productCategories = get_the_terms( get_the_id(), 'product_cat' );
		//  var_dump($jsonCategories);
		foreach ( $productCategories as $category ) {

				array_push($jsonProductCategories, $category->name);
		
		}

		$productJson = [

			'@context'		=>	'http://schema.org',
			'@type'			=>	'Product',
			'name'			=>	$product->get_title(),
			'offers'		=>	[

				// schema.org/Offer
				'@context'		=>	'http://schema.org',
				'@type'			=>	'Offer',
				'availability'	=>	'https://schema.org/InStock',
				'price'			=>	$jsonPrice,
				'url'			=>	get_permalink(get_the_id()),
				'seller'		=>	$wwOrg,
				'offeredBy'		=>	$wwOrg,
				'description'	=>	$jsonDesc,
				'image'			=>	$schemaThumb[0],
				'priceCurrency'	=>	'USD',
				'category'		=>	$jsonProductCategories
				],

			'description'	=>	$jsonDesc,
			// 'priceCurrency'	=>	'USD'
		];

		?>
		<script type="application/ld+json">
			<?php echo json_encode($productJson); ?>

		</script>
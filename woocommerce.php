<?php
/**
 * The template for displaying all woocommerce pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<div class="wrapper" id="woocommerce-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

			<?php 

				$template_name = '\archive-product.php'; 
				$args = array(); 
				$template_path = ''; 
				$default_path = untrailingslashit( plugin_dir_path(__FILE__) ) . '\woocommerce';

					if ( is_singular( 'product' ) ) {

						woocommerce_content();

			//For ANY product archive, Product taxonomy, product search or /shop landing page etc Fetch the template override;
				} 	elseif ( file_exists( $default_path . $template_name ) )
					{
					wc_get_template( $template_name, $args, $template_path, $default_path );

			//If no archive-product.php template exists, default to catchall;
				}	else  {
								
					do_action( 'woocommerce_before_shop_loop' ); 

					?>


					<div style="width:100%" class="row">
					<?php TemplateEngine::get('/includes/ww_page_links.php', $posts, 'posts'); ?>
					<div class="col-sm-12"><div class="card-deck">
					
					<?php 
				foreach($posts as $post){
			
				?>
				<div class="col-md-4" width="100%;" style="margin-top:15px;">
					<?php TemplateEngine::get('/includes/product_in_loop.php', wc_get_product($post->ID), 'product'); ?>
					</div>
				<?php  }
				
				 ?>
	</div>
	</div>
	</div>
	<div class="col-md-4" width="100%;" style="margin-top:15px;">
			<?php TemplateEngine::get('/includes/ww_page_links.php', $posts, 'posts'); ?>
			</div>
				 <?php 
				}

			;?>

			</main><!-- #main -->

		</div><!-- #primary -->

		<!-- Do the right sidebar check -->
		<?php if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) : ?>

			<?php get_sidebar( 'right' ); ?>
				
			
		<?php endif; ?>

	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
